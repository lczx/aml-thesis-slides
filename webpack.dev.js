const merge = require('webpack-merge');
const webpack = require('webpack');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = merge(require('./webpack.common'), {

  mode: 'development',
  devtool: 'inline-source-map',

  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          'style-loader',
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: [require('postcss-preset-env')]
            }
          },
          'sass-loader'
        ]
      }
    ]
  },

  devServer: {
    contentBase: false,
    hot: true
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new StyleLintPlugin({
      emitErrors: false,
      config: {
        extends: 'stylelint-config-standard',
        rules: {
          'declaration-empty-line-before': null,
          'no-descending-specificity': null, // Seems unusable w/ SASS
          'number-leading-zero': null,
          'rule-empty-line-before': null,
          'selector-list-comma-newline-after': null
        }
      }
    })
  ]

});
