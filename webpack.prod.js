const merge = require('webpack-merge');
const webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = merge(require('./webpack.common'), {

  mode: 'production',
  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '../'
            }
          },
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              plugins: [require('postcss-preset-env')]
            }
          },
          'sass-loader'
        ]
      }
    ]
  },

  output: {
    filename: 'assets/[name].[contenthash].js'
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: 'assets/[name].[contenthash].css'
    }),
    new webpack.HashedModuleIdsPlugin()
  ],

  optimization: {
    runtimeChunk: 'single',
    minimizer: [
      new UglifyJsPlugin({cache: true, parallel: true, sourceMap: true}),
      new OptimizeCSSAssetsPlugin({})
    ]
  }

});
