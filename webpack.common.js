const path = require('path');
const CleanPlugin = require('clean-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {

  entry: './src/index.js',

  module: {
    rules: [
      {
        test: /\.(ttf|otf|eot|svg|woff(2)?|png|mp4)$/,
        loader: 'file-loader',
        options: {
          name: 'assets/[hash].[ext]'
        }
      }
    ]
  },

  plugins: [
    new CleanPlugin(['dist']),
    new HtmlPlugin({
      title: 'Thesis Slides · Zanussi',
      template: '!!ejs-loader!./src/index.html',
      minify: {
        collapseWhitespace: true,
        preserveLineBreaks: false
      }
    })
    //new BundleAnalyzerPlugin()
  ],

  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },

  // Can use optimizations as of [https://webpack.js.org/plugins/split-chunks-plugin/] or better, Dynamic Imports
  optimization: {
    splitChunks: {
      chunks: 'all'
    }
  }

};
