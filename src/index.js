import './styles/style.scss'
import 'impress.js'
import {stepEnterListener as titleStepEnterListener} from './modules/titleChanger/titleChanger';
import {stepEnterListener as moreStepEnterListener} from "./modules/showMore/showMore";

/* Key mapping:
 *  A - Toggles "more" content
 *  Excape - Exits from "more"
 *  S - Toggles video playback on last slide
 *  D - Makes video on last slide fullscreeen
 */

const impressApi = impress();
const impressRoot = document.getElementById('impress');

const handler = function (e) {
  if (e.type === 'impress:stepleave') {
    console.log('Leaving step "' + e.target.id + '" (target hint: "' + e.detail.next.id + '")');
  } else if (e.type === 'impress:stepenter') {
    console.log('Entered step "' + e.target.id + '"');
    titleStepEnterListener(e);
    moreStepEnterListener(e);
  }
};

impressApi.lib.gc.addEventListener(impressRoot, 'impress:stepenter', handler);
impressApi.lib.gc.addEventListener(impressRoot, 'impress:stepleave', handler);
impressApi.init();


const demoVideo = document.querySelector('#outro > video');

// Initialize keyboard video controls
window.addEventListener('keyup', function (e) {
  if (!document.body.classList.contains('impress-on-outro'))
    return;

  if (e.key === 'd') {
    if (!demoVideo.classList.contains('fullscreen')) {
      if (demoVideo.mozRequestFullScreen) demoVideo.mozRequestFullScreen();
      else if (demoVideo.webkitRequestFullScreen()) demoVideo.webkitRequestFullScreen();
    } else {
      if (document.mozCancelFullScreen) document.mozCancelFullScreen();
      else if (document.webkitExitFullscreen) document.webkitExitFullscreen();
    }
    demoVideo.classList.toggle('fullscreen');
  }

  if (e.key === 's') {
    if (demoVideo.paused || demoVideo.stopped) {
      demoVideo.play();
    } else {
      demoVideo.pause();
    }
  }
});
