import './titleStyle.scss'

function recursiveGetSlideTitleNode(slideElement) {
  if (slideElement === null) return null;

  let titleNode = slideElement.getElementsByClassName('section-title')[0];
  if (titleNode !== undefined) return titleNode;

  return recursiveGetSlideTitleNode(slideElement.previousElementSibling);
}

function titleHasContent(titleElement) {
  return titleElement !== null && !titleElement.classList.contains('clear');
}

export function stepEnterListener(e) {
  const newTitle = recursiveGetSlideTitleNode(e.target);
  const titleContainer = document.getElementById('section-title-container');

  if (titleHasContent(newTitle) && newTitle.textContent === titleContainer.textContent) return;

  titleContainer.classList.add('hidden');
  setTimeout(() => {
    titleContainer.textContent = newTitle === null ? '' : newTitle.textContent;
    if (titleHasContent(newTitle))
      titleContainer.classList.remove('hidden');
  }, 500);
}
